import {Message} from 'discord.js';
import { Command } from '../command';

const name = 'ping';
const desc = 'Ping!';

export = class PingCommand extends Command {
  constructor() {
    super(name, desc);
  }

  async execute(msg: Message) {
    await msg.channel.send('Pong!');
  }
}