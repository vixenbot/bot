import { Message } from "discord.js";
import { Vixen } from "../../vixen";
import music from "../../utils/music";
import { Command, Syntax } from "../../command";

const name = 'volume';
const desc = 'Adjust player volume';
const syntax: Syntax[] = [
  {
    name: 'self',
    desc: 'Get the current audio volume',
    args: []
  },
  {
    name: 'adjustVol',
    desc: 'Set the audio volume',
    args: [
      {
        name: '<desired audio level (0-100)>',
        desc: 'Sets the player\'s audio volume to the desired level',
        examples: [
          '50'
        ]
      }
    ]
  }
]
const args = [
  {
    name: '<desired audio level (0-100)>',
    description: 'Sets the player\'s audio volume to the desired level',
    examples: [
      '50'
    ]
  }
]

export = class VolumeCommand extends Command {
  constructor() {
    super(name, desc, syntax);
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const maxVolume = 100;
    if (args[0]) {
      let properValue;
      if (args[0] as any as number > maxVolume) {
        properValue = maxVolume;
      } else {
        properValue = args[0] as any as number;
      }
      music.setVolume(vixen, msg.guild!, properValue);
      msg.channel.send('Set the volume to ' + properValue);
      vixen.log(`[${msg.guild!.id}] ${msg.author.tag}(${msg.author.id}) set the player volume to ${properValue}`);
    } else {
      msg.channel.send('The volume is currently set to ' + vixen.lavaClient.playerCollection.get(msg.guild!.id)?.volume);
    }
  }
}