import { Command } from "../../command";
import { Vixen } from "../../vixen";
import { Message } from "discord.js";

const name = 'skip';
const desc = 'Skip the current track';

export = class SkipCommand extends Command {
  constructor() {
    super(name, desc);
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    vixen.lavaClient.playerCollection.get(msg.guild!.id)!.stop();
  }
}