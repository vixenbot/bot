import {Message, TextChannel} from 'discord.js';
import { Vixen } from '../../vixen';
import musicUtils from '../../utils/music';
import { Command, Syntax } from '../../command';
import { LoadingMessage } from '../../loadingmessage';

const name = 'play';
const desc = 'Play music'
const syntax: Syntax[] = [
  {
    name: 'playMusic',
    desc: 'Play the specified URL or search query, or queues it if something is already playing',
    args: [
      {
        name: '<url or search query>',
        desc: 'URL or search query for the track to play',
        examples: [
          'never gonna give you up rick astley',
          'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
        ]
      }
    ]
  }
];

export = class PlayCommand extends Command {
  constructor() {
    super(name, desc, syntax, {argsReq: true});
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const loadMessage = new LoadingMessage(msg.channel as TextChannel, 'Please wait...', vixen);
    if (!msg.member!.voice) {
      msg.reply('You need to be in a voice channel to do that.');
      return;
    }
    const query = args.join(' ');
    vixen.log(`Lavalink query is '${query}'`, 'info', msg.guild!.id);

    const settings = await vixen.getGuildSettings(msg.guild!);

    const player = vixen.lavaClient.spawnPlayer({
      guild: msg.guild!,
      voiceChannel: msg.member?.voice.channel!,
      textChannel: msg.channel as TextChannel,
      volume: settings.get('volume'),
      deafen: true
    });

    const tracks = await player.lavaSearch(query, msg.member!, {
      source: 'yt',
      add: false
    });

    if (Array.isArray(tracks)) {
      const track = tracks[0];
      player.queue.add(track);
      if (!player.playing) player.play();
      else musicUtils.sendQueueEmbed(msg.channel as TextChannel, track, vixen.lavaClient);
      loadMessage.remove();
    }
  }
}