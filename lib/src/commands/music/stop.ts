import { Message } from "discord.js";
import { Vixen } from "../../vixen";
import { Command } from "../../command";

const name = 'stop';
const desc = 'Stop playing music';

export = class StopCommand extends Command {
  constructor() {
    super(name, desc);
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    vixen.lavaClient.playerCollection.get(msg.guild!.id)!.destroy();
  }
}