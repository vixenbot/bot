import { Command, Syntax } from "../../command";
import { Message } from "discord.js";
import { Vixen } from "../../vixen";

const name = 'loop';
const desc = 'Toggle loop on the queue or current track';
const syntax: Syntax[] = [
  {
    name: 'loopOne',
    desc: 'Toggles loop on the currently playing track',
    args: [
      {
        name: 'one',
        desc: '',
        examples: []
      }
    ]
  },
  {
    name: 'loopAll',
    desc: 'Toggles loop on the entire queue',
    args: [
      {
        name: 'all',
        desc: '',
        examples: []
      }
    ]
  }
]
const args = [
  {
    name: 'one',
    description: 'Toggles loop on the currently playing track'
  },
  {
    name: 'all',
    description: 'Toggles loop on the entire queue'
  }
];

export = class LoopCommand extends Command {
  constructor() {
    super(name, desc, syntax, {argsReq: true});
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const player = vixen.lavaClient.playerCollection.get(msg.guild!.id)!;

    // Loop the current track only
    if (args[0] === 'one') {
      let status;
      if (player.queue.repeatTrack) status = player.queue.toggleRepeat();
      else status = player.queue.toggleRepeat('track');
      msg.channel.send('Track loop ' + (status ? 'enabled' : 'disabled'));
      vixen.log(`${msg.author.tag}(${msg.author.id}) ${status ? 'enabled' : 'disabled'} track loop`, 'info', msg.guild!.id);
    }
    
    // Loop all tracks in the queue
    else if (args[0] === 'all') {
      let status;
      if (player.queue.repeatQueue) status = player.queue.toggleRepeat();
      else status = player.queue.toggleRepeat('queue');
      msg.channel.send('Queue loop ' + (status ? 'enabled' : 'disabled'));
      vixen.log(`${msg.author.tag}(${msg.author.id}) ${status ? 'enabled' : 'disabled'} queue loop`, 'info', msg.guild!.id);
    }
  }
}