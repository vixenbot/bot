import { Message } from "discord.js";
import { Vixen } from "../../vixen";
import { Command } from "../../command";

const name = 'pause';
const desc = 'Pause/unpause the current track';

export = class PauseCommand extends Command {
  constructor() {
    super(name, desc);
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const player = vixen.lavaClient.playerCollection.get(msg.guild!.id)!;
    if (player.paused) player.resume();
    else player.pause();
  }
}