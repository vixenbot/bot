import { Command, Syntax } from "../command";
import { Message, TextChannel } from "discord.js";
import { Vixen } from "../vixen";

const name = 'help';
const desc = 'Get help for different commands';
const syntax: Syntax[] = [
  {
    name: 'self',
    desc: 'Display basic help for all commands',
    args: []
  },
  {
    name: 'specificCommand',
    desc: 'Get help for a specific command',
    args: [
      {
        name: '<command>',
        desc: 'The command to get help for',
        examples: [
          'play'
        ]
      }
    ]
  }
]

export = class HelpCommand extends Command {
  constructor() {
    super(name, desc, syntax);
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    vixen.commandHandler.getHelp(msg.channel as TextChannel, args[0]);
  }
}