import { Command, Syntax } from "../../command";
import { Message } from "discord.js";
import { Vixen } from "../../vixen";
import { table, TableUserConfig } from 'table';
import moment from "moment";

const name = 'muted';
const desc = 'Shows a table of the muted users in the current guild';

export = class MutedCommand extends Command {
  constructor() {
    super(name, desc, undefined, {modOnly: true});
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const guild = msg.guild!;
    const guildData = await vixen.getGuildData(guild);
    const muted = guildData.mutedUsers;
    let messageString;
    let data = [['ID', 'On', 'For', 'Until']];
    const tableCfg: TableUserConfig = {
      border: {
        topBody: ``,
        topJoin: ``,
        topLeft: ``,
        topRight: ``,

        bottomBody: ``,
        bottomJoin: ``,
        bottomLeft: ``,
        bottomRight: ``,

        bodyLeft: ``,
        bodyRight: ``,
        bodyJoin: `|`,

        joinBody: `─`,
        joinLeft: ``,
        joinRight: ``,
        joinJoin: `┼`,
      },
      columns: {
        0: {
          alignment: 'left',
        },
        1: {
          alignment: 'left',
        },
        2: {
          alignment: 'left',
        },
        3: {
          alignment: 'left',
        },
        4: {
          alignment: 'left',
        },
      },
      drawHorizontalLine: (index: number) => {
        return index === 1;
      },
    };
    let page = 1;
    if (muted.size <= 0) return await msg.channel.send('No one on this server is muted.');
    muted.forEach((person, id) => {
      data.push([`${person.name} (${id})`, moment.unix(person.startTime).utc().format('DD/MM/YYYY HH:mm [UTC]'), moment.duration(moment.unix(person.endTime).diff(moment.unix(person.startTime))).humanize(), moment.unix(person.endTime).utc().format('DD/MM/YYYY HH:mm [UTC]')]);
      if (`Muted users page ${page}\`\`\`${table(data, tableCfg)}\`\`\``.length >= 2000) {
        const overflow = data.pop()!;
        msg.channel.send(`Muted users page ${page}\`\`\`${table(data, tableCfg)}\`\`\``);
        page++;
        data = [['Discord Tag', 'ID', 'Muted On', 'Muted For', 'Muted Until'], overflow];
      }
    });
    messageString = table(data, tableCfg);
    await msg.channel.send(`Muted users page ${page}\`\`\`${messageString}\`\`\``);
  }

}