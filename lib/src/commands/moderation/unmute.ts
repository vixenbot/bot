import { Command, Syntax } from "../../command";
import { Message, Role } from "discord.js";
import { Vixen } from "../../vixen";

const name = 'unmute';
const desc = 'Unmute a user';
const syntax: Syntax[] = [
  {
    name: 'userUnmute',
    desc: 'Mute a user',
    args: [
      {
        name: '<user to unmute>',
        desc: 'The @mention or Discord ID of the user to unmute',
        examples: [
          '%%BOTMENTION%%',
          '%%BOTID%%'
        ]
      }
    ]
  }
]

export = class UnmuteCommand extends Command {
  constructor() {
    super(name, desc, syntax, {modOnly: true});
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    if (args[0]) {
      const guild = msg.guild!;
      const guildData = await vixen.getGuildData(guild);
      const user = guild.member(args[0].replace(/[^A-Za-z0-9]/g, ''));
      if (!user) return;
      const muted = guildData.mutedUsers;
      const nick = user.nickname ? `Nickname: ${user.nickname}` : '';
      if (muted.has(user.id)) {
        const muteRole = guildData.roles.get('muted');
        if (!muteRole) return;
        await user.roles.remove(muteRole, `Manually unmuted by ${msg.author.tag}`);
        muted.delete(user.id);
        await vixen.db.collection('guilds').updateOne({id: guild.id}, {$set: {mutedUsers: muted}});
        try {
          await user.user.send(`You are no longer muted on ${user.guild.name}. Remember to follow the rules!`);
        } catch {
          vixen.log('Unable to send DM to ' + user.user.tag, 'error');
        }
        await msg.channel.send(`User \`${user.user.tag} (${nick}ID: ${user.id})\` manually unmuted.`);
      } else {
        await msg.channel.send(`User \`${user.user.tag} (${nick}ID: ${user.id})\` is not muted.`);
      }
    }
  }
}