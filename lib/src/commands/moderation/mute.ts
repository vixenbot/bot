import { Command, Syntax } from "../../command";
import { Message, Role, User } from "discord.js";
import { Vixen } from "../../vixen";
import moment from 'moment';

const name = 'mute';
const desc = 'Mute a user';
const syntax: Syntax[] = [
  {
    name: 'userMute',
    desc: 'Mute a user',
    args: [
      {
        name: '<user to mute>',
        desc: 'The @mention or Discord ID of the user to mute',
        examples: [
          '%%BOTMENTION%%',
          '%%BOTID%%'
        ]
      },
      {
        name: '<amount of time>',
        desc: 'The amount of time to mute the user for',
        examples: [
          '15 minutes',
          '5 days'
        ]
      }
    ]
  }
];

export = class MuteCommand extends Command {
  constructor() {
    super(name, desc, syntax, {modOnly: true});
  }

  async execute(msg: Message, args: string[], vixen: Vixen) {
    const collection = vixen.db.collection('guilds');
    const guildData = await collection.findOne({id: msg.guild!.id});
    let muteRole: Role;
    if (guildData.roles && guildData.roles.muted) {
      vixen.log('Retrieving mute role ID from DB', 'debug');
      muteRole = guildData.roles.muted;
    } else {
      vixen.log('Creating mute role for ' + msg.guild?.id, 'debug');
      muteRole = await msg.guild!.roles.create({
        data: {
          name: 'Muted',
          color: 'GREY',
          mentionable: false,
        },
        reason: 'Muted role is required fo use of the mute command'
      });
      await collection.updateOne({id: msg.guild!.id}, {$set: {'roles.muted': muteRole!.id}});
      msg.guild?.channels.cache.forEach(channel => {
        channel.updateOverwrite(muteRole, {
          ADD_REACTIONS: false,
          SEND_MESSAGES: false,
          CONNECT: false,
          SPEAK: false
        }).catch(() => {
          vixen.log(`Unable to set muted role permissions on #${channel.name}. Do I have access to that channel?`, 'error', msg.guild!.id);
        });
      });
    }
    
    const person = msg.guild?.member(args[0].replace(/[^A-Za-z0-9]/g, ''));

    const currentTime = moment();
    const muteEndTime = moment().add(args[1], args[2] as any);

    if (!person) {
      await msg.channel.send('That user isn\'t a member of this server, or the command syntax is incorrect.');
    } else {
      const muted = guildData.mutedUsers ? new Map(Object.entries(guildData.mutedUsers)) : new Map();
      let nick = person.nickname ? `Nickname: ${person.nickname}, ` : '';
      if (muted.has(person.id)) {
        await msg.channel.send(`That user is already muted. Their mute will expire ${moment.unix(muted.get(person.id).muteTimeEnd).fromNow()}`);
      } else {
        const mutedData = {
          name: person.user.tag,
          startTime: currentTime.unix(),
          endTime: muteEndTime.unix()
        };
        muted.set(person.id, mutedData);
        collection.updateOne({id: msg.guild?.id}, {$set: {mutedUsers: muted}}, {upsert: true});
        await msg.channel.send(`Muted user \`${person.user.tag} (${nick}ID: ${person.id})\` until ${moment.unix(muteEndTime.unix()).calendar()}.`);
        vixen.log(`Muted user \`${person.user.tag} (${nick}ID: ${person.id})\` until ${moment.unix(muteEndTime.unix()).calendar()}.`, 'info', msg.guild!.id);
        try {
          await person.user.send(`Whoop! Someone didn't follow the rules on ${person.guild.name}! You have been muted on it until ${moment.unix(muteEndTime.unix()).format('DD/MM/YYYY HH:mm [UTC]')}`);
        } catch {
          vixen.log('Unable to send DM to ' + person.user.tag, 'error');
        }
        await person.roles.add(muteRole);
        await person.voice.kick('User has been muted.');
      }
    }
  }
}