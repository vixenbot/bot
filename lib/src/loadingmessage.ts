import { TextChannel, Message, GuildEmoji} from "discord.js";
import { Vixen } from "./vixen";

export class LoadingMessage {
  message!: Message;
  loadingEmoji!: GuildEmoji;
  constructor(channel: TextChannel, message: string, vixen: Vixen) {
    this.create(channel, message, vixen);
  }

  async create(channel: TextChannel, message: string, vixen: Vixen) {
    const emojis = await vixen.getEmojis(channel.guild);
    this.loadingEmoji = channel.guild.emojis.resolve(emojis!.get('loading')!)!;
    this.message = await channel.send(`${this.loadingEmoji} ${message}`);
  }

  async update(newMessage: string) {
    this.message.edit(`${this.loadingEmoji} ${this.message}`);
  }

  async remove() {
    this.message.delete();
  }
}