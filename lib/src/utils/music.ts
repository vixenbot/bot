import { TextChannel, MessageEmbed, Guild } from "discord.js";
import { Track, LavaClient, Player } from "@anonymousg/lavajs";
import formatDuration from 'format-duration';
import { Vixen } from "../vixen";
const ColorThief = require('colorthief');

export = {
  async setVolume(vixen: Vixen, guild: Guild, level: number) {
    vixen.lavaClient.playerCollection.get(guild.id)?.setVolume(level);
    vixen.db.collection('guilds').updateOne({id: guild.id}, {$set: {'options.volume': level}}, {upsert: true});
  },

  async sendNPEmbed(channel: TextChannel, track: Track) {
    const embed = new MessageEmbed();
    embed.setTitle('DJ Vixen');
    embed.setDescription('Playing video requested by ' + track.user.displayName);
    const colorData = await this.getThumbnailColorData(track.thumbnail.max);
    embed.setColor(colorData.dominant);
    embed.setThumbnail(track.user.user.avatarURL()!);
    embed.addField('Title', track.title);
    embed.addField('Author', track.author, true);
    embed.addField('Duration', formatDuration(track.length), true);
    embed.setImage(track.thumbnail.max);
    embed.setURL(track.uri);
    await channel.send(embed);
  },

  async sendQueueEmbed(channel: TextChannel, track: Track, lavaClient: LavaClient) {
    const embed = new MessageEmbed();
    embed.setTitle('DJ Vixen');
    embed.setDescription(track.user.displayName + ' added a track to the queue');
    const colorData = await this.getThumbnailColorData(track.thumbnail.max);
    embed.setColor(colorData.dominant);
    embed.setThumbnail(track.thumbnail.max);
    embed.addField('Title', track.title);
    embed.addField('Author', track.author, true);
    embed.addField('Duration', formatDuration(track.length), true);
    embed.addField('ETA', this.getTimeTil(lavaClient, channel.guild, track));
    embed.setURL(track.uri);
    await channel.send(embed);
  },

  getTimeTil(lavaClient: LavaClient, guild: Guild, track: Track) {
    const player = lavaClient.playerCollection.get(guild.id);
    const queue = player?.queue;
    const elapsed = player?.position;
    let totalDuration = queue!.toArray()[0].length - elapsed!;
    totalDuration += queue?.duration!;
    return formatDuration(queue?.duration! - track.length - elapsed!);
  },

  async getThumbnailColorData(thumbnailUrl: string): Promise<{dominant: [number, number, number], palette: [number, number, number][]}> {
    const colorData = {
        dominant: await ColorThief.getColor(thumbnailUrl),
        palette: await ColorThief.getPalette(thumbnailUrl)
    };
    return colorData;
}

}