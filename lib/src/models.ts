import { Snowflake } from "discord.js";

export interface GuildData {
  id: string,
  name: string,
  emojis: Map<string, Snowflake>,
  options: Map<string, string>,
  roles: Map<string, Snowflake>,
  mutedUsers: Map<Snowflake, {name: string, startTime: number, endTime: number}>
}