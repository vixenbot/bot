import { Collection, Guild, MessageEmbed, Channel, TextChannel } from "discord.js";
import { Command } from "./command";
import { Vixen } from "./vixen";

export class CommandHandler {
  private vixen: Vixen;
  commands: Collection<string, Command>;
  groups: { name: string; description: string; enabled: boolean; }[];
  constructor(groups: { name: string; description: string; enabled: boolean; }[], vixen: Vixen) {
    this.vixen = vixen;
    this.commands = new Collection<string, Command>();
    this.groups = groups;
  }

  async getHelp(channel: TextChannel, command?: string) {
    const embed = new MessageEmbed();
    embed.setTitle('Vixen Command Reference');
    const commands = [];
    this.commands.forEach(command => commands.push(command.name));
    const guildPrefix = (await this.vixen.getGuildSettings(channel.guild)).get('prefix');

    // Get help for a specific command
    if (command) {
      if (this.commands.has(command)) {
        const commandString = guildPrefix + command;
        const commandData = this.commands.get(command);
        let descriptionString = `**${commandString}**: ${commandData?.description}\n\n**Syntax:**`;
        if (commandData!.syntaxes.size > 0) {
          commandData?.syntaxes.forEach(syntax => {
            const examples = commandData.examples(syntax.name);
            let syntaxString = '';
            if (syntax.name !== 'self') {
              syntaxString += '**Arguments:**';
              syntax.args.forEach(arg => {
                syntaxString += `\n\`${arg.name}\`: ${arg.desc}`;
              });
            }
            syntaxString += '\n**Syntax:**\n`';
              syntaxString += `${commandString}`
              syntax.args.forEach(arg => {
                syntaxString += ` ${arg.name}`;
              });
              syntaxString += '`';
            if (examples && examples.length > 0) {
              syntaxString += '\n**Examples:**\n';
              examples.forEach(example => {
                syntaxString += `\`${example}\`\n`;
              });
            }
            const botMention = `@${this.vixen.bot.user?.username}#${this.vixen.bot.user?.discriminator}`;
            syntaxString = syntaxString.replace(/%%COMMAND%%/g, commandString);
            syntaxString = syntaxString.replace(/%%BOTMENTION%%/g, botMention);
            syntaxString = syntaxString.replace(/%%BOTID%%/g, this.vixen.bot.user?.id!);
            embed.addField(syntax.desc, syntaxString, true);
          });
        } else {
          embed.addField(commandData?.description, `\`${commandString}\``);
        }
        //   let argString = arg.description;
        //   if (arg.subArgs) {
        //     argString += '\n\nProperties:\n';
        //     arg.subArgs.forEach(subArg => {
        //       argString += `**${subArg.name}**: ${subArg.description}`;
        //     });
        //   }
        //   if (examples) {
        //     argString += '\n\n**Examples:**\n';
        //     examples.forEach(example => {
        //       argString += `\`${commandString} ${example}`;
        //       if (arg.subArgs) {
        //         arg.subArgs.forEach(subArg => {
        //           if (subArg.examples) {
        //             subArg.examples.forEach(subExample => {
        //               argString += ` ${subExample}\`\n`;
        //             })
        //           }
        //           argString += `\`\n`;
        //         })
        //       } else {
        //         argString += `\`\n`;
        //       }
        //     });
        //   }
        //   embed.addField(arg.name, argString);
        // });
        // if (commandData?.args.size! <= 0) descriptionString += ' None';
        // embed.setDescription(descriptionString);
      } else {
        channel.send('No help available for *' + command + '*');
      }
    }
    
    // Get general info for all commands
    else {
      this.commands.forEach(command => {
        const commandString = guildPrefix + command.name;
        embed.addField(commandString, command.description);
      });
    }
    channel.send(embed);
  }
}