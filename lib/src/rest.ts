import express from 'express';
import session from 'express-session';
import passport from 'passport';
import Strategy from 'passport-discord';
import { Vixen } from "./vixen";
import { Guild } from 'discord.js';
import music from './utils/music';
import { Track } from '@anonymousg/lavajs';
import { GuildData } from './models';

interface RESTTrack extends Track {
  colorData: {
    dominant: [number, number, number],
    palette: [number, number, number][]
  }
}

const app = express();

const scopes = ['identify', 'guilds'];
const prompt = 'consent';

export class REST {
  vixen: Vixen;
  main!: import("http").Server;
  constructor(vixen: Vixen) {
    this.vixen = vixen;

    this.run();
  }

  async run() {
    passport.serializeUser((user, done) => {
      done(null, user);
    });

    passport.deserializeUser((obj, done) => {
      done(null, obj);
    });

    passport.use(new Strategy({
      clientID: this.vixen.config.webClient.id,
      clientSecret: this.vixen.config.webClient.secret,
      callbackURL: 'http://localhost:9090/callback'
    }, (accessToken, refreshToken, profile, done) => {
      process.nextTick(() => {
        return done(null, profile);
      });
    }));
    
    app.use(session({
      secret: 'keyboard cat',
      resave: false,
      saveUninitialized: false
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.get('/oauth', passport.authenticate('discord', {scope: scopes, prompt: prompt}));

    app.get('/callback', passport.authenticate('discord', {failureRedirect: '/'}), (req, res) => {
      res.redirect('/');
    });

    app.get('/logout', checkAuth, async (req, res) => {
      req.logOut();
      res.redirect('/');
    });

    app.get('/api/status', checkAuth, async (req: any, res) => {
      const managedGuilds: {name: string, id: string}[] = [];
      this.vixen.bot.guilds.cache.forEach(guild => {
        const member = guild.members.resolve(req.user.id);
        if (member?.permissions.has('MANAGE_GUILD')) {
          managedGuilds.push({
            name: guild.name,
            id: guild.id
          });
        }
      });
      const response = {
        authed: true,
        userData: req.user,
        managedGuilds: managedGuilds
      };
      res.send(response);
    });

    app.get('/api/guilds', checkAuth, async (req, res) => {
      const guilds: Guild[] = [];
      this.vixen.bot.guilds.cache.forEach(guild => {
        guilds.push(guild);
      });
      res.send(guilds);
    });

    app.get('/api/guilds/:id', checkAuth, async (req, res) => {
      interface RESTGuildInfo {
        persistent: GuildData,
        player?: {
          queue: {
            tracks: RESTTrack[],
            repeat: {
              track: boolean,
              queue: boolean
            },
            size: number,
            duration: number,
            empty: boolean
          },
          trackStartTime: number,
          playerStartTime: number,
          position: number,
          volume: number,
          paused: boolean,
          playing: boolean
        }
      }
      const persistentInfo = await this.vixen.getGuildData(req.params.id);
      const playerInfo = this.vixen.lavaClient.playerCollection.has(req.params.id) ? this.vixen.lavaClient.playerCollection.get(req.params.id) : null;
      const guildInfo: RESTGuildInfo = {} as RESTGuildInfo;
      if (playerInfo) {
        const trackArray = playerInfo.queue.toArray();
        const restTracks: RESTTrack[] = [];
        const thumbColorDataPromises: Promise<any>[] = [];
        trackArray.forEach(track => {
          thumbColorDataPromises.push(music.getThumbnailColorData(track.thumbnail.max));
        });
        const promiseResults = await Promise.all(thumbColorDataPromises);
        trackArray.forEach((track, index) => {
          const restTrack: RESTTrack = track as RESTTrack;
          restTrack.colorData = promiseResults[index];
          restTracks.push(restTrack);
        });
        guildInfo.player = {
          queue: {
            tracks: restTracks,
            repeat: {
              track: playerInfo?.queue.repeatTrack,
              queue: playerInfo?.queue.repeatQueue
            },
            size: playerInfo?.queue.size,
            duration: playerInfo?.queue.duration,
            empty: playerInfo?.queue.empty
          },
          trackStartTime: this.vixen.extraPlayerData.get(req.params.id)!.trackStartTime,
          playerStartTime: this.vixen.extraPlayerData.get(req.params.id)!.playerStartTime,
          position: playerInfo?.position,
          volume: playerInfo?.volume,
          paused: playerInfo?.paused,
          playing: playerInfo?.playing
        }
      }
      guildInfo.persistent = persistentInfo;
      res.send(guildInfo);
    });

    this.main = app.listen(3000, () => {
      this.vixen.log('REST API is listening on port 3000');
    });
  }

  stop() {
    this.main.close();
  }
}

function checkAuth(req: any, res: any, next: any) {
  if (req.isAuthenticated()) return next();
  res.send({authed: false});
}