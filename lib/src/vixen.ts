import logger from 'loglevel';
import * as prefix from 'loglevel-plugin-prefix';
import chalk from 'chalk';
import {Chance} from 'chance';

import { MongoClient, Db } from 'mongodb';
import { Client, Collection, Guild, DiscordAPIError, GuildMember, User, Emoji, GuildEmoji, Snowflake } from 'discord.js';
import { readdirSync, fstat, ensureDirSync, readFileSync } from 'fs-extra';
import {REST} from './rest';
import {Command} from './command';
import * as path from 'path';
import randomItem from 'random-item';
import {LavaClient} from '@anonymousg/lavajs';
import musicUtils from './utils/music';

import death from 'death';
import { CommandHandler } from './commandhandler';
import moment from 'moment';
import { GuildData } from './models';
import stringify from 'json-stringify-safe';

const colors: {[key: string]: any} = {
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red,
};

const nodes = [
  {
    host: 'lavalink',
    port: 2333,
    password: 'keyboardcat'
  }
]

prefix.reg(logger);
logger.enableAll();
prefix.apply(logger, {
  format(level, name, timestamp) {
    return `${chalk.gray(`[${timestamp}]`)} ${colors[level.toUpperCase()](level + ':')}`;
  },
});

export class Vixen {
  db: Db;
  mongoClient: MongoClient;
  rootDir: string;
  config: { token: string; owner: string; webClient: {id: string; secret: string;}};
  guildsData: Collection<unknown, unknown>;
  bot!: Client;
  commandHandler!: CommandHandler;
  api!: REST;
  lavaClient!: LavaClient;
  chance: Chance.Chance;
  extraPlayerData: Map<string, {playerStartTime: number, trackStartTime: number}>
  constructor(mongo: MongoClient, config: {token: string, owner: string, clientId: string, clientSecret: string}, root: string) {
    this.db = mongo.db('vixen');
    this.mongoClient = mongo;
    this.rootDir = root;
    this.config = {
      token: config.token,
      owner: config.owner,
      webClient: {
        id: config.clientId,
        secret: config.clientSecret
      }
    };
    this.guildsData = new Collection();
    this.chance = new Chance();
    this.extraPlayerData = new Map();
    // Create client
    this.bot = new Client();

    this.start();
  }

  async getGuildData(guild: Guild | Snowflake): Promise<GuildData> {
    const dbData = await this.db.collection('guilds').findOne({id: guild instanceof Guild ? guild.id : guild});
    return {
      id: dbData.id as string,
      name: dbData.name as string,
      emojis: dbData.emojis ? new Map(Object.entries(dbData.emojis)) : new Map(),
      options: dbData.options ? new Map(Object.entries(dbData.options)) : new Map(),
      roles: dbData.roles ? new Map(Object.entries(dbData.roles)) : new Map(),
      mutedUsers: dbData.mutedUsers ? new Map(Object.entries(dbData.mutedUsers)) : new Map()
    }
  }

  later(delay: number): Promise<void> {
    return new Promise(resolve => {
      setTimeout(resolve, delay);
    });
  }

  log(message: string, level = 'info', guild?: string): void {
    const logMessage = guild ? `${chalk.cyan(`[${guild}]`)} ${message}` : message;
    switch (level.toLowerCase()) {
      case 'debug':
        logger.debug(logMessage);
        break;
      case 'info':
        logger.info(logMessage);
        break;
      case 'warn':
        logger.warn(logMessage);
        break;
      case 'error':
        logger.error(logMessage);
        break;
    }
  }

  async start() {
    this.log('Starting Vixen');

    // Check config
    if (!this.config.token) {
      this.log(`Discord token has not been set!`, 'error');
      return process.exit(1);
    }
    if (!this.config.owner) this.log('Bot owner ID not set!', 'warn');

    // Register commands
    const commandGroups = [
      {
        name: 'fun',
        description: 'Fun commands or just-for-the-heck-of-it commands',
        enabled: true
      },
      {
        name: 'moderation',
        description: 'Commands related to guild moderation',
        enabled: true
      },
      {
        name: 'music',
        description: 'Commands related to controlling the audio player',
        enabled: true
      }
    ]
    this.commandHandler = new CommandHandler(commandGroups, this);

    // Register ungrouped commands
    const commandFiles = readdirSync(path.join(__dirname, 'commands')).filter(file => file.endsWith('.ts'));
    commandFiles.forEach(async file => {
      const command: Command = await import(`./commands/${file}`);
      this.registerCommand(command);
    });

    // Register grouped commands
    this.commandHandler.groups.forEach(group => {
      const groupCommands = readdirSync(path.join(__dirname, 'commands', group.name)).filter(file => file.endsWith('.ts'));
      groupCommands.forEach(async file => {
        const command: Command = await import(`./commands/${group.name}/${file}`);
        this.registerCommand(command);
      });
    });

    this.bot.once('ready', async () => {
      this.api = new REST(this);
      this.lavaClient = new LavaClient(this.bot, nodes);
      this.log('Logged into Discord', 'info');
      this.bot.user?.setActivity('Hello world!');

      this.bot.guilds.cache.forEach(async guild => {
        ensureDirSync(path.join('data', guild.id));
        const guilds = this.db.collection('guilds');
        await guilds.updateOne({id: guild.id}, {
          $set: {
            id: guild.id,
            name: guild.name,
          },
        }, {upsert: true});
        const guildData = await guilds.findOne({id: guild.id});
        const emojis: Map<string, string> = guildData.emojis ? new Map(Object.entries(guildData.emojis)) : new Map();
        if (!emojis.has('loading') || guild.emojis.resolve(emojis.get('loading')!)) {
          this.log(`Loading emoji is incorrect on guild ${guild.id}. Searching for an existing one...`, 'warn');
          const existing = await this.findExistingEmoji(guild, 'vixenload');
          let newEmoji: GuildEmoji;
          if (existing) {
            this.log(`Found existing loading emoji on guild ${guild.id}, setting it...`, 'warn');
            newEmoji = existing;
          } else {
            this.log(`No loading emoji exists on guild ${guild.id}, creating one...`, 'warn');
            newEmoji = await guild.emojis.create(path.join(this.rootDir, 'assets/loading.gif'), 'vixenload');
          }
          emojis.set('loading', newEmoji.id);
          await guilds.updateOne({id: guild.id}, {$set: {emojis: emojis}});
        }
        if (!guildData.options) {
          await guilds.updateOne({id: guild.id}, {
            $set: {
              options: {
                volume: 35,
                prefix: '//',
              }
            },
          }, {upsert: true});
        }
      });

      this.lavaClient.on('queueOver', player => {
        this.log(`[${player.options.guild.id}] Queue is empty, disconnecting`);
        player.destroy();
      });

      this.lavaClient.on('trackPlay', (track, player) => {
        const extraPlayerData = this.extraPlayerData.get(player.options.guild.id);
        if (player.queue.repeatTrack) {
          this.log(`Repeating track: ${track.title}`, 'info', player.options.guild.id);
        } else {
          this.log(`Playing next track: ${track.title}`, 'info', player.options.guild.id);
          musicUtils.sendNPEmbed(player.options.textChannel, track);
        }
        if (!extraPlayerData) this.extraPlayerData.set(player.options.guild.id, {playerStartTime: Date.now(), trackStartTime: Date.now()});
        else this.extraPlayerData.get(player.options.guild.id)!.trackStartTime = Date.now();
      })

      this.bot.setInterval(() => {
        const statusesFile = readFileSync(path.join(this.rootDir, 'assets/statuses.txt'), 'utf-8');
        const statuses = statusesFile.split('\n');
        
        // Add dynamic statuses here
        
        const selectedStatus = statuses[this.chance.integer({min: 0, max: statuses.length-1})];
        this.bot.user?.setActivity(selectedStatus);
      }, 60000);

      this.bot.setInterval(async () => {
        const guilds = this.db.collection('guilds').find({});
        guilds.forEach((server) => {
          if (server.mutedUsers) {
            const muted = new Map(Object.entries(server.mutedUsers));
            if (muted.size > 0) {
              this.log(`${server.id} contains ${muted.size} muted users, checking for expired mutes`, 'debug');
              muted.forEach(async (person: any, id) => {
                if (moment.unix(person.endTime).isBefore(moment())) {
                  this.log(`${person.name}(${id})'s mute has expired. Unmuting.`, 'info', server.id);
                  const guild = this.bot.guilds.resolve(server.id);
                  const muteRole = (await this.db.collection('guilds').findOne({
                    id: guild!.id,
                  })).roles.muted;
                  guild!.member(id)!.roles.remove(muteRole, 'User mute time expired.');
                  guild!.member(id)!.user.send(`You are no longer muted on ${guild!.name}. Remember to follow the rules!`)
                  .catch(() => {
                    this.log('Unable to send DM to ' + guild!.member(id)!.user.tag, 'error');
                  });
                  const newMuted = new Map(Object.entries(server.mutedUsers));
                  newMuted.delete(id);
                  await this.db.collection('guilds').updateOne({id: guild!.id}, {
                    $set: {
                      mutedUsers: newMuted,
                    },
                  });
                }
              });
            }
          }
        });
      }, 5000);
  

      this.log('Vixen startup complete');
    });

    this.bot.on('message', async msg => {
      if (msg.guild === null) return;
      const guild = msg.guild.id;
      const guildData = await this.db.collection('guilds').findOne({id: guild});
      const prefix = guildData.options.prefix;

      if (msg.mentions.users.has(this.bot.user!.id)) {
        msg.react(randomItem(['😄', '🤗', '😊', '🙃', '🦊']));
      }

      if (!msg.content.startsWith(prefix) || msg.author.bot) return;

      const args = msg.content.slice(prefix.length).split(/ +/);
      if (!args || args.length === 0) return msg.channel.send('No args provided');
      const msgCmd = args.shift()!.toLowerCase();

      if (!this.commandHandler.commands.has(msgCmd)) return;
      const command = this.commandHandler.commands.get(msgCmd);
      const executable = canExecute(command!, msg.member!, this.config.owner);
      if (!executable.value) return msg.reply(executable.error);
      
      try {
        command!.run(msg, args, this);
      } catch (error) {
        this.log(error, 'error');
        msg.reply('An error ocurred executing that command, please check the log for details.');
      }
    });

    // Graceful exit
    death(async () => {
      console.log('');
      this.log('Shuttind down Vixen...');
      await this.mongoClient.close();
      this.log('Closed DB connection');
      this.api.stop();
      this.log('Stopped REST API and OAuth Provider');
      this.bot.destroy();
      this.log('Disconnected from Discord');
      await this.later(1500);
      this.log('Vixen has been shut down');
      console.log('');
      process.exit(0);
    });

    this.bot.login(this.config.token);
  }

  registerCommand(command: any) {
    let register = new command.default(this) as Command;
    this.commandHandler.commands.set(register.name, register);
  }

  async findExistingEmoji(guild: Guild, emojiName: string): Promise<false | GuildEmoji> {
    for (const emoji of guild.emojis.cache.array()) {
      const author = await emoji.fetchAuthor();
      if ((author.id === this.bot.user!.id) && (emoji.name === emojiName)) {
        return emoji;
      }
    }
    return false;
  }

  async getEmojis(guild: Guild) {
    const query = await this.db.collection('guilds').findOne({id: guild.id});
    const emojis = query.emojis ? new Map<string, string>(Object.entries(query.emojis)) : undefined;
    return emojis;
  }

  async getGuildSettings(guild: Guild) {
    const query = await this.db.collection('guilds').findOne({id: guild.id});
    const settings = query.options ? new Map(Object.entries(query.options)) : new Map();
    return settings;
  }
}

function canExecute(command: Command, member: GuildMember, vixenOwner: string): {value: boolean, error: string} {
  if (command.modOnly && !isUserModerator(member)) return {
    value: false,
    error: 'You need to be a moderator to use that command.'
  };
  else if (command.adminOnly && !isUserAdmin(member)) return {
    value: false,
    error: 'You need to have the \'Administrator\' permission to use that command.'
  };
  else if (command.ownerOnly && !isUserOwner(member)) return {
    value: false,
    error: 'Only the server owner may use that command.'
  };
  else if (command.masterOnly && !isUserMaster(member, vixenOwner)) return {
    value: false,
    error: 'Only Vixen\'s owner may use that command.'
  };
  else return {
    value: true,
    error: 'None'
  };
}

function isUserModerator(member: GuildMember) {
  return member.hasPermission('KICK_MEMBERS') || member.hasPermission('BAN_MEMBERS') || isUserOwner(member);
}

function isUserAdmin(member: GuildMember) {
  return member.hasPermission('ADMINISTRATOR') || isUserOwner(member);
}

function isUserOwner(member: GuildMember) {
  return member.guild.owner === member;
}

function isUserMaster(member: GuildMember, vixenOwner: string) {
  return member.user.id === vixenOwner;
}