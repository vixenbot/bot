import { Message, TextChannel, Guild } from "discord.js";
import { Vixen } from "./vixen";

export interface Syntax {
  name: string,
  desc: string,
  args: Argument[],
}

interface CommandOptions {
  argsReq?: boolean,
  modOnly?: boolean,
  adminOnly?: boolean,
  ownerOnly?: boolean,
  masterOnly?: boolean
}

interface Argument {
  name: string;
  desc: string;
  examples: string[];
}

export abstract class Command {
  public readonly name: string;
  public readonly description: string;
  public readonly syntaxes: Map<string, Syntax>;
  public readonly modOnly: boolean | undefined;
  public readonly adminOnly: boolean | undefined;
  public readonly ownerOnly: boolean | undefined;
  public readonly masterOnly: boolean | undefined;
  public readonly argsReq: boolean | undefined;

  constructor(name: string, description: string, syntaxes?: Syntax[], options?: CommandOptions) {
    this.name = name;
    this.description = description;
    this.syntaxes = new Map();
    if (syntaxes) {
      syntaxes.forEach(syntax => this.syntaxes.set(syntax.name, syntax));
    }
    this.argsReq = options?.argsReq;
    this.modOnly = options?.modOnly;
    this.adminOnly = options?.adminOnly;
    this.ownerOnly = options?.ownerOnly;
    this.masterOnly = options?.masterOnly;
  };

  run(msg: Message, args: string[], vixen: Vixen) {
    if (this.argsReq && args.length < 1) {
      this.sendUsage(msg.channel as TextChannel, vixen);
    } else this.execute(msg, args, vixen);
  }
  abstract async execute(msg: Message, args: string[], vixen: Vixen): Promise<any>;

  examples(syntax: string): string[] | false {
    if (!this.syntaxes.has(syntax)) return false;
    const selectedSyntax = this.syntaxes.get(syntax)!;
    const examples: string[] = [];
    if (selectedSyntax.args.length > 0) {
      for (let i = 0; i < selectedSyntax.args[0].examples.length; i++) {
        const exampleString: string[] = [];
        exampleString.push('%%COMMAND%%');
        selectedSyntax.args.forEach(arg => {
          exampleString.push(arg.examples[i]);
        });
        examples.push(exampleString.join(' '));
      }
    }

    return examples;
  }

  // Construct string to define command usage
  async sendUsage(channel: TextChannel, vixen: Vixen) {
    vixen.commandHandler.getHelp(channel, this.name);
  }
  //   const settings = await vixen.getGuildSettings(channel.guild);
  //   const command = settings.get('prefix') + this.name;
  //   let usage = [
  //     command + ': ' + this.description
  //   ];
  //   if (this.args) {
  //     usage.push('');
  //     usage.push('Usage:');
  //     this.args.forEach(arg => {
  //       usage.push(command + ' ' + arg.name + ': ' + arg.description);
  //     });
  //   }
  //   let usageString = '```\n';
  //   usageString += usage.join('\n');
  //   usageString += '```';
  //   channel.send(usageString);
  // }
}