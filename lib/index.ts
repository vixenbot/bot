import {ensureDirSync} from 'fs-extra';
import {MongoClient} from 'mongodb';
import {Vixen} from './src/vixen';

const client = new MongoClient('mongodb://db', {useUnifiedTopology: true});

ensureDirSync('./cache');
ensureDirSync('./data');

start();

async function start() {
  await client.connect();
  const config = await import('../config.json');
  new Vixen(client, config, './');
}